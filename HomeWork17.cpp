﻿
#include <iostream>

class MyClass
{
 private:
    int a, b, c;
 public:
     MyClass() : a(0), b(1), c(0)
     {}
     MyClass(int a1, int b1, int c1): a(a1), b(b1), c(c1)
     {}
     void Print()
     {
         std::cout << a << " " << b << " " << c << "\n";
     }
};

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;
    double module;
public:
    Vector() : x(5), y(5), z(5)
    {}
    Vector(double x1, double y1, double z1) : x(x1), y(y1), z(z1)
    {}
    void Show()
    {
        module = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        std::cout << "Vector: " << x << " " << y << " " << z << "\n";
        std::cout << "Vector module: " << module << "\n";
    }
};
int main()
{
    MyClass temp{};
    temp.Print();
    MyClass tempNew{1,0,1};
    tempNew.Print();

    Vector v;
    v.Show();
    
}